package com.example.task2

import android.app.Activity
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import android.content.Intent
import kotlinx.android.synthetic.main.secondactivity.*

class Main2Activity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.secondactivity)
        init()
    }

    private fun init() {
        val intent = intent
        val userModel = intent.getParcelableExtra("data") as UserModel
        nameTextEditor.setText(userModel.name)
        surnameTextEditor.setText(userModel.surname)
        mailTextEditor.setText(userModel.email)
        yearTextEditor.setText(userModel.year.toString())
        genderTextEditor.setText(userModel.gender)
        saveButton.setOnClickListener{
            openFirstActivity(userModel)
        }

    }

    private fun openFirstActivity(userModel: UserModel) {
        val intent = intent
        intent.putExtra("name", nameTextEditor.text.toString())
        intent.putExtra("surname", surnameTextEditor.text.toString())
        intent.putExtra("email", mailTextEditor.text.toString())
        intent.putExtra("year", yearTextEditor.text.toString().toInt())
        intent.putExtra("gender", genderTextEditor.text.toString())
        setResult(Activity.RESULT_OK, intent)
        finish()
    }

}
