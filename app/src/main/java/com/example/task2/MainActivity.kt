package com.example.task2

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.app.Activity
import android.content.Intent
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity() {

    private val sendermenderi = 7

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        init()
    }

    private fun init() {
        editButton.setOnClickListener{
            openSecondActivity()
        }

    }

    private fun openSecondActivity() {
        val userModel = UserModel (
            nameTextView.text.toString(),
            surnameTextView.text.toString(),
            mailTextView.text.toString(),
            yearTextView.text.toString().toInt(),
            genderTextView.text.toString()

        )
        val user = Intent(
            this,Main2Activity::class.java
        )

        user.putExtra("data", userModel)
        startActivityForResult(user, sendermenderi)

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (resultCode == Activity.RESULT_OK && requestCode == sendermenderi) {
            val name = data!!.extras!!.getString("name", "")
            val surname = data!!.extras!!.getString("surname", "")
            val email = data!!.extras!!.getString("email", "")
            val year = data!!.extras!!.getInt("year", 0)
            val gender = data!!.extras!!.getString("gender", "")
            nameTextView.text = name.toString()
            surnameTextView.text = surname.toString()
            mailTextView.text = email.toString()
            yearTextView.text = year.toString()
            genderTextView.text = gender.toString()
        }

        super.onActivityResult(requestCode, resultCode, data)
    }
}
